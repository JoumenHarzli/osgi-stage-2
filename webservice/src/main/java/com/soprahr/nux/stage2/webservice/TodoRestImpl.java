package com.soprahr.nux.stage2.webservice;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soprahr.nux.stage2.reader.TodoService;

public class TodoRestImpl implements TodoRest {

	private static final Logger logger = LoggerFactory
			.getLogger(TodoRestImpl.class);
	TodoService todoService;

	public void setTodoService(TodoService todoService) {
		this.todoService = todoService;
	}

	@Override
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listDictionnaire() {
		HashMap<String, Object> dict = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();
		String jsonContent;
		dict = todoService.getDictionnaire();
		try {
			jsonContent = (String) mapper.writeValueAsString(dict);
			return Response.ok(jsonContent, MediaType.APPLICATION_JSON).build();
		} catch (JsonProcessingException e) {
			logger.error("Unable to convert Map to json");
			logger.error(e.toString());
			return Response.status(Status.NOT_FOUND).build();
		}
	}

}
