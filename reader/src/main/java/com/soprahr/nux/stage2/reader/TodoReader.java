package com.soprahr.nux.stage2.reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sopraht.nux.stage2.models.Todo;

public class TodoReader implements TodoService {

	private static final Logger logger = LoggerFactory
			.getLogger(TodoReader.class);

	int maxtodos;

	public void setMaxtodos(int maxtodos) {
		this.maxtodos = maxtodos;
	}

	private List<Todo> readTodos() {
		String parsedContent;
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		if (bundle != null) {
			try {
				parsedContent = IOUtils.toString(
						bundle.getResource("todos.json"), "UTF-8");
				ObjectMapper mapper = new ObjectMapper();
				List<Todo> todos = mapper.readValue(
						parsedContent,
						mapper.getTypeFactory().constructCollectionType(
								List.class, Todo.class));
				if (todos != null) {
					return todos.subList(0, maxtodos);
				}
			} catch (IOException e) {
				logger.error("unable to parse file");
				logger.error(e.toString());
			}
		}
		return new ArrayList<>();
	}

	@Override
	public HashMap<String, Object> getDictionnaire() {
		HashMap<String, Object> dict = new HashMap<>();
		dict.put("maxtodos", maxtodos);
		dict.put("todos", readTodos());
		return dict;
	}

}
