package com.soprahr.nux.stage2.reader;

import java.util.HashMap;

public interface TodoService {

	HashMap<String, Object> getDictionnaire();

}